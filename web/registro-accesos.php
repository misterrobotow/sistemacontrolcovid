<?php
    session_start();
    if(!isset($_SESSION['rol']))
    {
        header('Location:login.php');
    }
    else{
        include('database.php');

        $records = $connection->prepare('SELECT COUNT(id_ingreso) AS num_accesos FROM acceso');
        $records->execute();
        $num_accesos = $records->fetch(PDO::FETCH_ASSOC);

        $records = $connection->prepare('SELECT persona.id_persona,persona.nombre, persona.rol, control_points.descripcion, control_points.facultad, acceso.registed_at FROM acceso,persona,control_points WHERE acceso.id_persona = persona.id_persona AND acceso.id_control_point = control_points.id_control_point');
        $records->execute();
        $accesos = $records->fetchAll();
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Control de acceso FI UAEMex</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootswatch/4.5.2/litera/bootstrap.min.css"></link>
</head>
<body>
    <!--Navbar-->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="">
          <img src="img/uaemex_logo.png" alt="UAEMex">
          UAEMex
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarColor03">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link" href="#">Inicio <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Caracteristicas</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Acerca de</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><?=$_SESSION['username']?></a>
              <div class="dropdown-menu">
                <a class="dropdown-item" href="#">Configuracion de la cuenta</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="logout.php">Cerrar sesion</a>
              </div>
            </li>
          </ul>
          <ul class="navbar-nav ml-auto">
          </ul>
        </div>
    </nav>
    <!--Navbar-->

    <div class="container px-0 mx-0">
        <div class="row">
            <div class="col-md-3 bg-light d-flex flex-column">
              <div class="mt-4 d-flex align-items-center justify-content-center">
                <!--<h4 class=""><?=$_SESSION['rol']?></h4><br>-->
              </div>      
              <div class="mt-4 d-flex align-items-center justify-content-center">
                <a href="cp-list.php">Puntos de control</a>
              </div>
              <div class="mt-4 d-flex align-items-center justify-content-center">
                <a href=""><strong>Registro de accesos</strong></a>
              </div>
              <!--
              <div class="mt-4 d-flex align-items-center justify-content-center">
                <a href="registro-accesos.php">Registrar a una persona</a>
              </div>
              -->
            </div>
            <div class="col-md-9">
                <h3 class="mt-4 mb-4 ml-4">Registro de accesos</h3>
                <div class="mb-4">
                    <p class="lead ml-4">Accessos registrados: <strong><?= $num_accesos['num_accesos']?> personas</strong></p>
                </div>
                <div class="mb-4 d-flex flex-row">
                    <input  class="form-control ml-4" type="text" id="search-input" placeholder="Buscar por nombre o identificador">
                    
                    <button type="button" class="btn btn-info" id="search-btn">BUSCAR</button>
                </div>
                <table class="table table-hover ml-4 pl-4" style="max-width:100%">
                    <thead>
                        <tr class="table-active">
                        <th scope="col">IDENTIFICADOR</th>
                          <th scope="col">PERSONA</th>
                          <th>ROL</th>
                          <th scope="col">PUNTO DE CONTROL ACCESADO</th>
                          <th>FACULTAD</th>
                          <th scope="col">FECHA Y HORA DEL REGISTRO</th>
                        </tr>
                    </thead>
                    <tbody id="table-body">
                        <?php for ($i=0; $i < count($accesos); $i++) { ?>
                            <tr>
                              <td><?= $accesos[$i]['id_persona']?></td>
                              <td><?= $accesos[$i]['nombre']?></td>
                              <td><?= $accesos[$i]['rol']?></td>
                              <td><?= $accesos[$i]['descripcion']?></td>
                              <td><?= $accesos[$i]['facultad']?></td>
                              <td><?= $accesos[$i]['registed_at']?></td>
                            </tr>
                        <?php };?>
                    </tbody>  
                </table>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    <script src="js/registro-accesos-func.js"></script>
</body>
</html>